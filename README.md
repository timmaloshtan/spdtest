## Description

https://spdtest-763ae.firebaseapp.com/editor/offices

Go to Profile Editor > Offices. This is an example of single page application to edit information about the company, its offices in particular. Offices can be added, edited and removed. Information is persisted to Firebase Realtime database.

React is used for view and Redux is utilized for app state management.
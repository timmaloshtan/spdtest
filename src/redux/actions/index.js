// @flow
import { getIsFetching } from '../reducers';
import constants from '../constants';
import { getOffices } from '../reducers';
import * as api from '../../api';
import type { Dispatch as ReduxDispatch } from 'redux';
import type { StateType, ActionType } from '../reducers';
import type { OfficeType } from '../../api';

export type MyDispatch = ReduxDispatch<StateType, ActionType>;
export type GetStateType = (void) => (StateType);

export const fetchOffices = () => (dispatch: MyDispatch, getState: GetStateType) => {
  if (getIsFetching(getState())) {
    return Promise.resolve();
  }

  dispatch({
    type: constants.FETCH_OFFICES_REQUEST,
  });

  return api.fetchOffices().then(
    response => {
      dispatch({
        type: constants.FETCH_OFFICES_SUCCESS,
        response,
      });
    },
    error => {
      dispatch({
        type: constants.FETCH_OFFICES_FAILURE,
        message: error.message || 'Something went wrong.'
      });
    }
  );
};

export const addOffice = () => (dispatch: MyDispatch, getState: GetStateType) => {
  dispatch(stopEditing());

  api.addOffice().then(response => {
    const currentOffices = getOffices(getState());
    dispatch({
      type: constants.FETCH_OFFICES_SUCCESS,
      response: [
        ...currentOffices,
        response
      ]
    });
  });
}

export const removeOffice = (id: string, reason: string, note: ?string) => (dispatch: MyDispatch) =>
  api.removeOffice(id, reason, note).then(response => {
    dispatch({
      type: constants.REMOVE_OFFICE_SUCCESS,
      response
    });
  });

export const editOffice = (id: string) => (dispatch: MyDispatch) => {
  dispatch(stopEditing());

  dispatch({
    type: constants.EDIT_OFFICE,
    id
  })
};

export const stopEditing = (id: ?string, dummy: ?boolean) => (dispatch: MyDispatch) => {
  if (dummy && id) {
    dispatch(removeOffice(id, 'Canceled Creation', null));
  } else {
    dispatch ({
      type: constants.STOP_EDITING
    });
  }
};


export const saveEditing = (office: OfficeType) => (dispatch: MyDispatch) => {
  dispatch(stopEditing());

  delete office.dummy;

  api.saveEditing(office.id, office).then(response => {
    dispatch({
      type: constants.SAVE_EDITING_SUCCESS,
      office: response
    });
  });
}
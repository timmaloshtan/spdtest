// @flow
import { combineReducers } from 'redux';
import constants from '../constants';
import type { OfficeType } from '../../api';

export type StateType = {
  offices: Array<OfficeType>,
  isFetching: boolean,
  errorMessage: string,
};

export type ActionType = {
  type: string,
  [propertyName: string]: any,
};

const offices = (state: Array<OfficeType> = [], action: ActionType) => {
  switch (action.type) {
    case constants.FETCH_OFFICES_SUCCESS:
      return [
        ...action.response
      ];
    case constants.REMOVE_OFFICE_SUCCESS:
      return state.filter(office => office.id !== action.response);
    case constants.EDIT_OFFICE:
      return state.map(office => {
        if (office.id === action.id) {
          office.isEdited = true;
        }
        return office;
      });
    case constants.STOP_EDITING:
      return state.map(office => {
        office.isEdited = false;
        return office;
      });
    case constants.SAVE_EDITING_SUCCESS:
      return state.map(office => (
        office.id === action.office.id ? action.office : office
      ));
    default:
      return state;
  }
};

const isFetching = (state: boolean = false, action) => {
  switch (action.type) {
    case constants.FETCH_OFFICES_REQUEST:
      return true;
    case constants.FETCH_OFFICES_SUCCESS:
    case constants.FETCH_OFFICES_FAILURE:
      return false;
    default:
      return state;
  }
}

const errorMessage = (state: ?string = null, action) => {
  switch (action.type) {
    case constants.FETCH_OFFICES_FAILURE:
      return action.message;
    case constants.FETCH_OFFICES_SUCCESS:
    case constants.FETCH_OFFICES_REQUEST:
      return null;
    default:
      return state;
  }
}

export default combineReducers({
  offices,
  isFetching,
  errorMessage
});

export const getOffices = (state: StateType): Array<OfficeType> => (state.offices);
export const getIsFetching = (state: StateType): boolean => (state.isFetching);
export const getErrorMessage = (state: StateType): string => (state.errorMessage);
import * as React from 'react';

import Header from '../Header';
import Content from '../Content';

class App extends React.Component {
  render() {
    return (
      <div className="Layout">
        <Header />
        <Content />
      </div>
    );
  }
}

export default App;
// @flow
import * as React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

import './Header.css'

const Header = () => (
  <div className="Header">
    <Navbar fluid collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="/"> </a>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <NavItem href="/editor">
            Profile Editor
          </NavItem>
        </Nav>
        <Nav pullRight>
          <NavItem href="/faq">
            FAQs
            </NavItem>
          <NavItem href="/contact">
            Contact
            </NavItem>
          <NavItem href="/">
            Save and Exit
            </NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </div>
);

export default Header;
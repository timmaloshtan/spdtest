// @flow
import * as React from 'react';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import AddressFormGroup from '../AddressFormGroup';
import ContactsFormGroup from '../ContactsFormGroup';
import type { OfficeType } from '../../api';

import './OfficeEditor.css';

const getLabel = (formfield: string): string => {
  const labels = {
    country: '*Country:',
    stateProvince: '*State/Province:',
    postalCode: '*Postal Code:',
    city: '*City:',
    streetAddress1: '*Street Address:',
    streetAddress2: 'Address 2:',
    phone: 'Phone:',
    fax: 'Fax:',
    email: 'Email:',
    primaryHQ: 'Office Type:'
  };

  return labels[formfield];
};

type Props = {
  office: OfficeType,
  onChange: Event => void,
  onSubmit: Event => void,
  onCancel: (string, boolean) => (mixed)
};

class OfficeEditor extends React.Component<Props> {
  render() {
    const office = JSON.parse(JSON.stringify(this.props.office));
    if (!office) {
      return null;
    }
    const { address, contacts, primaryHQ } = office;
    const { onChange, onSubmit, onCancel } = this.props;
    return (
      <div className="OfficeEditor">
        <Panel>
          <Panel.Body>
            <Grid fluid>
              <Row className="show-grid no-gutters">
                  <Col xs={12} sm={4}>
                    <AddressFormGroup
                      address={address}
                      onChange={onChange}
                      getLabel={getLabel}
                    />
                  </Col>
                  <Col xs={12} sm={6}>
                    <ContactsFormGroup
                      contacts={contacts}
                      onChange={onChange}
                      getLabel={getLabel}
                      primaryHQ={primaryHQ}
                    />
                  </Col>
                  <Col xs={12} sm={2}>
                    <Button className="step" type="button" onClick={() => onCancel(office.id, office.dummy)}>Cancel</Button>
                    <Button bsStyle="primary" onClick={onSubmit}>Save</Button>
                  </Col>
              </Row>
            </Grid>
          </Panel.Body>
        </Panel>
      </div>
    );
  }
}

export default OfficeEditor;
// @flow
import * as React from 'react';
import loadingIndicator from '../../assets/images/loadIndicator.gif';

import './LoadingIndicator.css'

const LoadingIndicator = ({ height }: {height: number}) => (
  <img 
    src={loadingIndicator}
    alt="Loading..."
    style={{height: `${height}px`, width: `${height/3*4}px`}}
  />
);

export default LoadingIndicator;
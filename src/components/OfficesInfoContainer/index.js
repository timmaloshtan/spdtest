// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { getOffices, getIsFetching, getErrorMessage } from '../../redux/reducers';
import { fetchOffices, addOffice } from '../../redux/actions';
import OfficesInfo from '../OfficesInfo';
import type { OfficeType } from '../../api';
import typeof { addOffice as AddOffice, fetchOffices as FetchOffices} from '../../redux/actions';

type Props = {
  officeToEdit: OfficeType,
  offices: Array<OfficeType>,
  isFetching: boolean,
  errorMessage: string,
  onRetry: FetchOffices,
  fetchOffices: FetchOffices,
  addOffice: AddOffice,
};

class OfficesInfoContainer extends React.Component<Props> {
  componentWillMount() {
    this.fetchData()
  }

  fetchData() {
    this.props.fetchOffices()
  }

  render() {
    return(
      <OfficesInfo {...this.props} />
    );
  }
}

const mapStateToProps = (state) => ({
  officeToEdit: getOffices(state).find(office => (office.isEdited)),
  offices: getOffices(state),
  isFetching: getIsFetching(state),
  errorMessage: getErrorMessage(state),
});

export default connect(
  mapStateToProps,
  {
    fetchOffices,
    onRetry: fetchOffices,
    addOffice
  }
)(OfficesInfoContainer);
// @flow
import * as React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import type { ContactsType } from '../../api/index';

const capitalize = (string: string): string =>
  (string[0].toUpperCase() + string.slice(1));

type Props = {
  contacts: ContactsType,
}

const OfficeContacts = ({ contacts }: Props) => {
  const filledKeys = Object.keys(contacts).filter(key =>
    (contacts[key])
  );
  return (
    <div className="wrapper">
      <div>
        {
          <Grid fluid>
            {
              filledKeys.map(key =>
                <Row key={key} className="show-grid no-gutters">
                  <Col xs={6} sm={5} md={4} className="right">
                    <p key={key}>{capitalize(key)}:</p>
                  </Col>
                  <Col xs={6} sm={7} md={8}>
                    <p key={key}>{contacts[key]}</p>
                  </Col>
                </Row>
              )
            }
          </Grid>
        }
      </div>
    </div>
  );
};

export default OfficeContacts;
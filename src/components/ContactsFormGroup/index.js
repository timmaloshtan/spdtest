// @flow
import * as React from 'react';
import { Col, Form, FormGroup, FormControl } from 'react-bootstrap';
import type { ContactsType} from '../../api';

type Props = {
  contacts: ContactsType,
  onChange: Event => void,
  getLabel: (string) => string;
  primaryHQ: boolean
};

const ContactsFormGroup = ({
  contacts,
  onChange,
  getLabel,
  primaryHQ
}: Props) => {

  return (
    <Form horizontal>
      {
        Object.keys(contacts).map((key) => (
          <FormGroup key={key} className="no-gutters">
            <Col className="right" sm={5}>
              <p>{getLabel(key)}</p>
            </Col>
            <Col sm={5}>
              <FormControl
                type="text"
                value={contacts[key]}
                name={key}
                onChange={onChange}
              />
            </Col>
          </FormGroup>
        ))
      }
      {
        <FormGroup className="no-gutters">
          <Col className="right" sm={5}>
            <p>{getLabel('primaryHQ')}</p>
          </Col>
          <Col sm={5}>
            <p>
              <input
                type="checkbox"
                checked={primaryHQ}
                name='primaryHQ'
                onChange={onChange}
              />  PrimaryHQ
            </p>
          </Col>
        </FormGroup>
      }
    </Form>
  );
};



export default ContactsFormGroup;
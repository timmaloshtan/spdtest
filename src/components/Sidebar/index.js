// @flow
import * as React from 'react';
import { Panel } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

import './Sidebar.css';

const Sidebar = () => (
  <div className="Sidebar">
    <div className="sidebar-header"></div>
    <Panel defaultExpanded>
      <Panel.Heading>
        <Panel.Title toggle>
          COMPANY INFO
        </Panel.Title>
      </Panel.Heading>
      <Panel.Collapse>
        <ul className="list-unstyled">
          <li><NavLink activeClassName="sidelink-active" to="/editor/basic">Basic Info</NavLink></li>
          <li><NavLink activeClassName="sidelink-active" to="/editor/offices">Offices</NavLink></li>
          <li><NavLink activeClassName="sidelink-active" to="/editor/competitors">Competitors</NavLink></li>
        </ul>
      </Panel.Collapse>
    </Panel>
    <Panel>
      <Panel.Heading>
        <Panel.Title toggle>
          MY FIRM
        </Panel.Title>
      </Panel.Heading>
      <Panel.Collapse>
        <ul className="list-unstyled">
          <li><NavLink activeClassName="sidelink-active" to="/editor/myfirm">My firm</NavLink></li>
        </ul>
      </Panel.Collapse>
    </Panel>
    <Panel>
      <Panel.Heading>
        <Panel.Title toggle>
          DEALS
        </Panel.Title>
      </Panel.Heading>
      <Panel.Collapse>
        <ul className="list-unstyled">
          <li><NavLink activeClassName="sidelink-active" to="/editor/deals">Deals</NavLink></li>
        </ul>
      </Panel.Collapse>
    </Panel>
    <Panel>
      <Panel.Heading>
        <Panel.Title toggle>
          FINANCIALS
        </Panel.Title>
      </Panel.Heading>
      <Panel.Collapse>
        <ul className="list-unstyled">
          <li><NavLink activeClassName="sidelink-active" to="/editor/financials">Financials</NavLink></li>
        </ul>
      </Panel.Collapse>
    </Panel>
  </div>
);

export default Sidebar;
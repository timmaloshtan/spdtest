// @flow
import * as React from 'react';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { editOffice } from '../../redux/actions'
import type { MyDispatch } from '../../redux/actions'
import OfficeAddress from '../OfficeAddress';
import OfficeContacts from '../OfficeContacts';
import RemovalModal from '../RemovalModal';
import type { OfficeType } from '../../api';

import './Office.css';

type Props = {
  office: OfficeType,
  dispatch: MyDispatch,
};

type State = {
  showRemovalModal: boolean
};

class Office extends React.Component<Props, State> {
  handleRemoveClick: void => void;

  constructor(props) {
    super(props);
    this.state = {
      showRemovalModal: false
    };
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
  }

  handleRemoveClick() {
    this.setState({
      showRemovalModal: true
    });
  }

  render() {
    const { office, dispatch } = this.props;
    return (
      <div className="Office">
        <Panel>
          <Panel.Body>
            <Grid fluid>
              <Row className="show-grid no-gutters">
                <Col xs={12} sm={4}>
                  <OfficeAddress {...office.address} hq={office.primaryHQ} />
                </Col>
                <Col xs={12} sm={5}>
                  <OfficeContacts contacts={office.contacts} />
                </Col>
                <Col xs={12} sm={3} className="right">
                  <Button className="remove-office" onClick={this.handleRemoveClick} >Remove</Button>
                  <Button
                    bsStyle="primary"
                    onClick={() => dispatch(editOffice(office.id))}
                  >Edit</Button>
                </Col>
              </Row>
            </Grid>
          </Panel.Body>
        </Panel>

        <RemovalModal
          id={office.id}
          show={this.state.showRemovalModal}
          onHide={() => this.setState({ showRemovalModal: false })}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  office: ownProps.office
})

export default connect(
  mapStateToProps,
  null
)(Office);
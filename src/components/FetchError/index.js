// @flow
import * as React from 'react';
import { Button } from 'react-bootstrap';
import typeof { fetchOffices } from '../../redux/actions';

type Props = {
  message: string,
  onRetry: fetchOffices,
}

const FetchError = ({ message, onRetry }: Props) => (
  <div>
    <p>
      Could not fetch new offices. {message}
      <Button bsStyle="primary" onClick={onRetry}>Retry</Button>
    </p>
  </div>
);

export default FetchError; 
// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import OfficeEditor from '../OfficeEditor';
import { getOffices } from '../../redux/reducers';
import { stopEditing, saveEditing } from '../../redux/actions';
import typeof { stopEditing as StopEditing, saveEditing as SaveEditing} from '../../redux/actions';
import type { OfficeType } from '../../api';

const setNestedLiteralProperty = (obj: Object, propName: string, value: string): void => {
  Object.keys(obj).forEach(key => {
    if (key === propName) {
      obj[key] = value;
    }
    if (typeof obj[key] === 'object') {
      setNestedLiteralProperty(obj[key], propName, value);
    }
  })
};

type Props = {
  office: OfficeType,
  stopEditing: StopEditing,
  saveEditing: SaveEditing,
}

class OfficeEditorContainer extends React.Component<Props> {
  office: OfficeType;
  handleChange: (Event | {label: string, value: string, name?: string}) => void;
  handleSubmit: (Event) => void

  constructor(props) {
    super(props);
    this.office = JSON.parse(JSON.stringify(props.office));

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillUpdate (nextProps) {
    if (this.office.id !== nextProps.office.id) {
      this.office = JSON.parse(JSON.stringify(nextProps.office))
    }
  }

  handleChange(e) {
    // accounting for custom shape of Select event
    if (e === null) {
      return;
    }
    if (e.hasOwnProperty('value')) {
      setNestedLiteralProperty(this.office, e.name, e.value);
    } else {
      const update = e.target.hasOwnProperty('checked') ?
                      e.target.checked :
                      e.target.value;
      setNestedLiteralProperty(this.office, e.target.name, update);
    }
    this.forceUpdate();
  }

  handleSubmit(e) {
    this.office.isEdited = false;
    this.props.saveEditing(this.office);
    e.preventDefault();
  }

  render() {
    return(
      <OfficeEditor
        office={this.office}
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
        onCancel={this.props.stopEditing}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  office: getOffices(state).find(office => (office.isEdited)),
});

export default connect(
  mapStateToProps,
  {
    stopEditing,
    saveEditing,
  }
)(OfficeEditorContainer);
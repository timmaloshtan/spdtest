// @flow
import * as React from 'react';
import Office from '../Office';
import type { OfficeType } from '../../api';

type Props = {
  offices: Array<OfficeType>
}

class OfficeList extends React.Component<Props> {
  render() {
    const offices = this.props.offices;
    return (
      <div className="OfficeList">
        {
          offices.filter(
            office => (!office.isEdited)
          ).map((office, i) =>
            <Office key={i} office={office} />
          )
        }
      </div>
    );
  }
}

export default OfficeList;